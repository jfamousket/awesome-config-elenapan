local naughty = require("naughty")
local icons = require("icons")
local notifications = require("notifications")

notifications.playerctl = {}

local notif
local first_time = true
local timeout = 2

local old_artist, old_song, paused
local send_playerctl_notif = function(song, artist)
    if first_time then
        first_time = false
    else
        if paused or (sidebar and sidebar.visible) or
            (client.focus and (client.focus.instance == "music" or client.focus.class == "music")) then
            -- Sidebar and already shows playerctl info, so
            -- destroy notification if it exists
            -- Also destroy it if music pauses
            if notif then
                notif:destroy()
            end
        else
            -- Since the signal is also emitted when seeking, only
            -- send a notification when the song and artist are different than
            -- before.
            if artist ~= old_artist and song ~= old_song then
                notif = notifications.notify_dwim({
                    title = "Now playing:",
                    message = "<b>" .. song .. "</b> by <b>" .. artist .. "</b>",
                    icon = icons.image.music,
                    timeout = timeout,
                    app_name = "playerctl"
                }, notif)
            end
        end
        old_artist = artist
        old_song = song
    end
end

awesome.connect_signal("bling::playerctl::status", function(status)
    paused = status
end)

-- Allow dynamically toggling playerctl notifications
notifications.playerctl.enable = function()
    awesome.connect_signal("bling::playerctl::title_artist_album", send_playerctl_notif)
    notifications.playerctl.enabled = true
end
notifications.playerctl.disable = function()
    awesome.disconnect_signal("bling::playerctl::title_artist_album", send_playerctl_notif)
    notifications.playerctl.enabled = false
end
notifications.playerctl.toggle = function()
    if notifications.playerctl.enabled then
        notifications.playerctl.disable()
    else
        notifications.playerctl.enable()
    end
end

-- Start with playerctl notifications enabled
notifications.playerctl.enable()
