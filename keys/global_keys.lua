local awful = require("awful")
local naughty = require("naughty")
local gears = require("gears")
local apps = require("apps")
local hotkeys_popup = require("awful.hotkeys_popup")
local helpers = require("helpers")

-- Mod keys
superkey = "Mod4"
altkey = "Mod1"
ctrlkey = "Control"
shiftkey = "Shift"

-- {{{ Key bindings
awful.keyboard.append_global_keybindings({awful.key({superkey}, "Return", function()
    awful.spawn(user.terminal)
end, {
    description = "open a terminal",
    group = "launcher"
}), awful.key({superkey, shiftkey}, "Return", function()
    awful.spawn("bash -c '" .. user.terminal .. " --working-directory `xcwd`'")
end, {
    description = "spawn a terminal with current path",
    group = "launcher"
}), awful.key({superkey, shiftkey}, "r", awesome.restart, {
    description = "reload awesome",
    group = "awesome"
}), awful.key({superkey, shiftkey}, "x", function()
    exit_screen_show()
end, {
    description = "quit awesome",
    group = "awesome"
}), awful.key({superkey}, "Escape", function()
    exit_screen_show()
end, {
    description = "quit awesome",
    group = "awesome"
}), awful.key({superkey}, "F1", hotkeys_popup.show_help, {
    description = "show help",
    group = "awesome"
}), awful.key({}, "XF86PowerOff", function()
    exit_screen_show()
end, {
    description = "quit awesome",
    group = "awesome"
}), awful.key({superkey}, "r", function()
    awful.spawn.with_shell("rofi -show drun")
end, {
    description = "rofi launcher",
    group = "launcher"
}), awful.key({superkey}, "d", function()
    -- Not all sidebars have a prompt
    if sidebar_activate_prompt then
        sidebar_activate_prompt("run")
    end
end, {
    description = "activate sidebar run prompt",
    group = "awesome"
}), awful.key({superkey}, "g", function()
    -- Not all sidebars have a prompt
    if sidebar_activate_prompt then
        sidebar_activate_prompt("web_search")
    end
end, {
    description = "activate sidebar web search prompt",
    group = "awesome"
}), awful.key({ctrlkey}, "space", function()
    awesome.emit_signal("elemental::dismiss")
    naughty.destroy_all_notifications()
end, {
    description = "dismiss notification",
    group = "notifications"
}), awful.key({}, "XF86MonBrightnessDown", function()
    awful.spawn.with_shell("brightnessctl set 5%-")
end, {
    description = "decrease brightness",
    group = "brightness"
}), awful.key({}, "XF86MonBrightnessUp", function()
    awful.spawn.with_shell("brightnessctl set +5%")
end, {
    description = "increase brightness",
    group = "brightness"
}), awful.key({}, "XF86AudioMute", function()
    helpers.volume_control(0)
end, {
    description = "(un)mute volume",
    group = "volume"
}), awful.key({}, "XF86AudioLowerVolume", function()
    helpers.volume_control(-5)
end, {
    description = "lower volume",
    group = "volume"
}), awful.key({}, "XF86AudioMicMute", function()
    awful.spawn.with_shell("pactl set-source-mute @DEFAULT_SOURCE@ toggle")
end, {
    description = "toggle microphone",
    group = "awesome"
}), awful.key({}, "XF86AudioRaiseVolume", function()
    helpers.volume_control(5)
end, {
    description = "raise volume",
    group = "volume"
}), awful.key({altkey}, "F1", function()
    helpers.volume_control(0)
end, {
    description = "(un)mute volume",
    group = "volume"
}), awful.key({altkey}, "F2", function()
    helpers.volume_control(-5)
end, {
    description = "lower volume",
    group = "volume"
}), awful.key({altkey}, "F3", function()
    helpers.volume_control(5)
end, {
    description = "raise volume",
    group = "volume"
}), awful.key({superkey}, "F12", apps.screenkey, {
    description = "raise volume",
    group = "volume"
}), awful.key({superkey}, "v", function()
    awful.spawn.with_shell("pactl set-source-mute @DEFAULT_SOURCE@ toggle")
end, {
    description = "(un)mute microphone",
    group = "volume"
}), awful.key({superkey, shiftkey}, "v", function()
    microphone_overlay_toggle()
end, {
    description = "toggle microphone overlay",
    group = "volume"
}), awful.key({}, "Print", function()
    apps.screenshot("full")
end, {
    description = "take full screenshot",
    group = "screenshots"
}), awful.key({superkey}, "Print", function()
    apps.screenshot("selection")
end, {
    description = "select area to capture",
    group = "screenshots"
}), awful.key({superkey, shiftkey}, "Print", function()
    apps.screenshot("clipboard")
end, {
    description = "select area to copy to clipboard",
    group = "screenshots"
}), awful.key({superkey}, "=", function()
    tray_toggle()
end, {
    description = "toggle tray visibility",
    group = "awesome"
}), awful.key({superkey}, "period", function()
    awful.spawn.with_shell("playerctl next")
end, {
    description = "next song",
    group = "media"
}), awful.key({}, "XF86AudioNext", function()
    awful.spawn.with_shell("playerctl next")
end, {
    description = "next song",
    group = "media"
}), awful.key({superkey}, "comma", function()
    awful.spawn.with_shell("playerctl previous")
end, {
    description = "previous song",
    group = "media"
}), awful.key({}, "XF86AudioPrev", function()
    awful.spawn.with_shell("playerctl previous")
end, {
    description = "previous song",
    group = "media"
}), awful.key({}, "XF86AudioPause", function()
    awful.spawn.with_shell("playerctl play-pause")
end, {
    description = "toggle pause/play",
    group = "media"
}), awful.key({}, "XF86AudioPlay", function()
    awful.spawn.with_shell("playerctl play-pause")
end, {
    description = "toggle pause/play",
    group = "media"
}), awful.key({superkey}, "space", function()
    awful.spawn.with_shell("playerctl play-pause")
end, {
    description = "toggle pause/play",
    group = "media"
}), awful.key({superkey, shiftkey}, "period", function()
    awful.spawn.with_shell("mpvc next")
end, {
    description = "mpv next song",
    group = "media"
}), awful.key({superkey, shiftkey}, "comma", function()
    awful.spawn.with_shell("mpvc prev")
end, {
    description = "mpv previous song",
    group = "media"
}), awful.key({superkey, shiftkey}, "space", function()
    awful.spawn.with_shell("mpvc toggle")
end, {
    description = "mpv toggle pause/play",
    group = "media"
}), awful.key({superkey}, "F8", function()
    awful.spawn.with_shell("mpvc quit")
end, {
    description = "mpv quit",
    group = "media"
}), awful.key({superkey}, "F7", function()
    awful.spawn.with_shell("freeze firefox")
end, {
    description = "send STOP signal to all firefox processes",
    group = "other"
}), awful.key({superkey, shiftkey}, "F7", function()
    awful.spawn.with_shell("freeze -u firefox")
end, {
    description = "send CONT signal to all firefox processes",
    group = "other"
}), awful.key({superkey}, "q", function()
    apps.scratchpad()
end, {
    description = "scratchpad",
    group = "launcher"
}), awful.key({superkey}, "F2", function()
    if dashboard_show then
        dashboard_show()
    end
end, {
    description = "dashboard",
    group = "custom"
}), awful.key({superkey}, "a", function()
    app_drawer_show()
end, {
    description = "App drawer",
    group = "custom"
}), awful.key({superkey}, "F3", apps.music, {
    description = "music client",
    group = "launcher"
}), awful.key({superkey}, "grave", function()
    sidebar_toggle()
end, {
    description = "show or hide sidebar",
    group = "awesome"
}), awful.key({superkey}, "b", function()
    wibars_toggle()
end, {
    description = "show or hide wibar(s)",
    group = "awesome"
}), awful.key({superkey}, "i", apps.markdown_input, {
    description = "markdown scratchpad",
    group = "launcher"
}), awful.key({superkey}, "e", apps.editor, {
    description = "editor",
    group = "launcher"
}), awful.key({superkey, shiftkey}, "e", function()
    awful.spawn.with_shell(gears.filesystem.get_configuration_dir() .. "scripts/rofi_edit")
end, {
    description = "quick edit file",
    group = "launcher"
}), awful.key({superkey}, "p", function()
    awful.spawn.with_shell("rofi-pass")
end, {
    description = "pick password",
    group = "launcher"
}), awful.key({superkey, shiftkey}, "m", function()
    awful.spawn.with_shell(gears.filesystem.get_configuration_dir() .. "scripts/dmscripts/dman")
end, {
    description = "find man page",
    group = "launcher"
}), awful.key({superkey, shiftkey}, "s", function()
    awful.spawn.with_shell(gears.filesystem.get_configuration_dir() .. "scripts/dmscripts/dmsearch")
end, {
    description = "search the web",
    group = "launcher"
}), awful.key({superkey, shiftkey}, "f", apps.file_manager, {
    description = "file manager",
    group = "launcher"
}), awful.key({superkey, shiftkey}, "p", apps.process_monitor, {
    description = "process monitor",
    group = "launcher"
}), awful.key({superkey, ctrlkey}, "j", function()
    awful.screen.focus_relative(1)
end, {
    description = "focus the next screen",
    group = "screen"
}), awful.key({superkey, ctrlkey}, "k", function()
    awful.screen.focus_relative(-1)
end, {
    description = "focus the previous screen",
    group = "screen"
})})
