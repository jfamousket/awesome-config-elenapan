local awful = require("awful")
local naughty = require("naughty")
local helpers = require("helpers")

-- Mod keys
superkey = "Mod4"
altkey = "Mod1"
ctrlkey = "Control"
shiftkey = "Shift"

awful.keyboard.append_global_keybindings({
    awful.key({
        superkey,
        shiftkey
    }, "minus", function()
        awful.tag.incgap(5, nil)
    end, {
        description = "increment gaps size for the current tag",
        group = "gaps"
    }),
    awful.key({
        superkey
    }, "minus", function()
        awful.tag.incgap(-5, nil)
    end, {
        description = "decrement gap size for the current tag",
        group = "gaps"
    }),
    awful.key({
        superkey,
        altkey
    }, "h", function()
        awful.tag.incnmaster(1, nil, true)
    end, {
        description = "increase the number of master clients",
        group = "layout"
    }),
    awful.key({
        superkey,
        altkey
    }, "l", function()
        awful.tag.incnmaster(-1, nil, true)
    end, {
        description = "decrease the number of master clients",
        group = "layout"
    }),
    awful.key({
        superkey,
        altkey
    }, "Left", function()
        awful.tag.incnmaster(1, nil, true)
    end, {
        description = "increase the number of master clients",
        group = "layout"
    }),
    awful.key({
        superkey,
        altkey
    }, "Right", function()
        awful.tag.incnmaster(-1, nil, true)
    end, {
        description = "decrease the number of master clients",
        group = "layout"
    }),
    awful.key({
        superkey,
        altkey
    }, "k", function()
        awful.tag.incncol(1, nil, true)
    end, {
        description = "increase the number of columns",
        group = "layout"
    }),
    awful.key({
        superkey,
        altkey
    }, "j", function()
        awful.tag.incncol(-1, nil, true)
    end, {
        description = "decrease the number of columns",
        group = "layout"
    }),
    awful.key({
        superkey,
        altkey
    }, "Up", function()
        awful.tag.incncol(1, nil, true)
    end, {
        description = "increase the number of columns",
        group = "layout"
    }),
    awful.key({
        superkey,
        altkey
    }, "Down", function()
        awful.tag.incncol(-1, nil, true)
    end, {
        description = "decrease the number of columns",
        group = "layout"
    }),
    awful.key({
        superkey
    }, "w", function()
        awful.layout.set(awful.layout.suit.max)
        helpers.single_double_tap(nil, function()
            local clients = awful.screen.focused().clients
            for _, c in pairs(clients) do
                c.floating = false
            end
        end)
    end, {
        description = "set max layout",
        group = "tag"
    }),
    awful.key({
        superkey
    }, "s", function()
        awful.layout.set(awful.layout.suit.tile)
        helpers.single_double_tap(nil, function()
            local clients = awful.screen.focused().clients
            for _, c in pairs(clients) do
                c.floating = false
            end
        end)
    end, {
        description = "set tiled layout",
        group = "tag"
    }),
    awful.key({
        superkey,
        shiftkey
    }, "s", function()
        awful.layout.set(awful.layout.suit.floating)
    end, {
        description = "set floating layout",
        group = "tag"
    })
})

-- Num row keybinds
awful.keyboard.append_global_keybindings({
    awful.key {
        modifiers = {
           superkey 
        },
        keygroup = "numrow",
        description = "only view tag",
        group = "tag",
        on_press = function(index)
            local screen = awful.screen.focused()
            local tag = screen.tags[index]
            if tag then
                tag:view_only()
            end
        end
    },
    awful.key {
        modifiers = {
            superkey,
            "Control"
        },
        keygroup = "numrow",
        description = "toggle tag",
        group = "tag",
        on_press = function(index)
            local screen = awful.screen.focused()
            local tag = screen.tags[index]
            if tag then
                awful.tag.viewtoggle(tag)
            end
        end
    },
    awful.key {
        modifiers = {
            superkey,
            "Shift"
        },
        keygroup = "numrow",
        description = "move focused client to tag",
        group = "tag",
        on_press = function(index)
            if client.focus then
                local tag = client.focus.screen.tags[index]
                if tag then
                    client.focus:move_to_tag(tag)
                end
            end
        end
    },
    awful.key {
        modifiers = {
            superkey,
            "Control",
            "Shift"
        },
        keygroup = "numrow",
        description = "toggle focused client on tag",
        group = "tag",
        on_press = function(index)
            if client.focus then
                local tag = client.focus.screen.tags[index]
                if tag then
                    client.focus:toggle_tag(tag)
                end
            end
        end
    },
    awful.key {
        modifiers = {
            superkey
        },
        keygroup = "numpad",
        description = "select layout directly",
        group = "layout",
        on_press = function(index)
            local t = awful.screen.focused().selected_tag
            if t then
                t.layout = t.layouts[index] or t.layout
            end
        end
    }
})
