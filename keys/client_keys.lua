local awful = require("awful")
local naughty = require("naughty")
local helpers = require("helpers")
local decorations = require("decorations")

-- Mod keys
superkey = "Mod4"
altkey = "Mod1"
ctrlkey = "Control"
shiftkey = "Shift"

-- Client management keybinds
client.connect_signal("request::default_keybindings", function()
    awful.keyboard.append_client_keybindings({awful.button({}, 1, function(c)
        client.focus = c
        if c.focusable then
            c:emit_signal("request::activate", "mouse_click", {
                raise = true
            })
        end
    end), awful.button({superkey}, 1, awful.mouse.client.move), awful.button({superkey}, 3, function(c)
        client.focus = c
        awful.mouse.client.resize(c)
        -- awful.mouse.resize(c, nil, {jump_to_corner=true})
    end), awful.key({superkey, "Shift"}, "f", function(c)
        c.fullscreen = not c.fullscreen
        c:raise()
    end, {
        description = "toggle fullscreen",
        group = "client"
    }), awful.key({superkey, shiftkey}, "c", function(c)
        c:kill()
    end, {
        description = "close",
        group = "client"
    }), awful.key({superkey, ctrlkey}, "Return", function(c)
        c:swap(awful.client.getmaster())
    end, {
        description = "move to master",
        group = "client"
    }), awful.key({superkey}, "o", function(c)
        c:move_to_screen()
    end, {
        description = "move to screen",
        group = "client"
    }), awful.key({superkey}, "n", function(c)
        -- The client currently has the input focus, so it cannot be
        -- minimized, since minimized clients can't have the focus.
        c.minimized = true
    end, {
        description = "minimize",
        group = "client"
    }), awful.key({superkey}, "m", function(c)
        c.maximized = not c.maximized
        c:raise()
    end, {
        description = "(un)maximize",
        group = "client"
    }), awful.key({superkey, ctrlkey}, "m", function(c)
        c.maximized_vertical = not c.maximized_vertical
        c:raise()
    end, {
        description = "(un)maximize vertically",
        group = "client"
    }), awful.key({superkey, ctrlkey}, "m", function(c)
        c.maximized_horizontal = not c.maximized_horizontal
        c:raise()
    end, {
        description = "(un)maximize horizontally",
        group = "client"
    }), awful.key({superkey}, "Down", function()
        awful.client.focus.bydirection("down")
        bling.module.flash_focus.flashfocus(client.focus)
    end, {
        description = "focus down",
        group = "client"
    }), awful.key({superkey}, "Up", function()
        awful.client.focus.bydirection("up")
        bling.module.flash_focus.flashfocus(client.focus)
    end, {
        description = "focus up",
        group = "client"
    }), awful.key({superkey}, "Left", function()
        awful.client.focus.bydirection("left")
        bling.module.flash_focus.flashfocus(client.focus)
    end, {
        description = "focus left",
        group = "client"
    }), awful.key({superkey}, "Right", function()
        awful.client.focus.bydirection("right")
        bling.module.flash_focus.flashfocus(client.focus)
    end, {
        description = "focus right",
        group = "client"
    }), awful.key({superkey}, "j", function()
        awful.client.focus.byidx(1)
    end, {
        description = "focus next by index",
        group = "client"
    }), awful.key({superkey}, "k", function()
        awful.client.focus.byidx(-1)
    end, {
        description = "focus previous by index",
        group = "client"
    }), awful.key({superkey, "Shift"}, "j", function()
        awful.client.swap.byidx(1)
    end, {
        description = "swap with next client by index",
        group = "client"
    }), awful.key({superkey, "Shift"}, "k", function()
        awful.client.swap.byidx(-1)
    end, {
        description = "swap with previous client by index",
        group = "client"
    }), awful.key({superkey}, "u", awful.client.urgent.jumpto, {
        description = "jump to urgent client",
        group = "client"
    }), awful.key({superkey}, "Tab", function()
        window_switcher_show(awful.screen.focused())
    end, {
        description = "activate window switcher",
        group = "client"
    }), awful.key({superkey, altkey}, "q", function()
        local clients = awful.screen.focused().clients
        for _, c in pairs(clients) do
            c:kill()
        end
    end, {
        description = "kill all visible clients for the current tag",
        group = "gaps"
    }), awful.key({superkey, ctrlkey}, "Down", function(c)
        helpers.resize_dwim(client.focus, "down")
    end), awful.key({superkey, ctrlkey}, "Up", function(c)
        helpers.resize_dwim(client.focus, "up")
    end), awful.key({superkey, ctrlkey}, "Left", function(c)
        helpers.resize_dwim(client.focus, "left")
    end), awful.key({superkey, ctrlkey}, "Right", function(c)
        helpers.resize_dwim(client.focus, "right")
    end), awful.key({superkey, ctrlkey}, "j", function(c)
        helpers.resize_dwim(client.focus, "down")
    end), awful.key({superkey, ctrlkey}, "k", function(c)
        helpers.resize_dwim(client.focus, "up")
    end), awful.key({superkey, ctrlkey}, "h", function(c)
        helpers.resize_dwim(client.focus, "left")
    end), awful.key({superkey, ctrlkey}, "l", function(c)
        helpers.resize_dwim(client.focus, "right")
    end), awful.key({superkey, shiftkey}, "n", function()
        local c = awful.client.restore()
        -- Focus restored client
        if c then
            client.focus = c
        end
    end, {
        description = "restore minimized",
        group = "client"
    }), awful.key({superkey, shiftkey}, "Down", function(c)
        helpers.move_client_dwim(c, "down")
    end), awful.key({superkey, shiftkey}, "Up", function(c)
        helpers.move_client_dwim(c, "up")
    end), awful.key({superkey, shiftkey}, "Left", function(c)
        helpers.move_client_dwim(c, "left")
    end), awful.key({superkey, shiftkey}, "Right", function(c)
        helpers.move_client_dwim(c, "right")
    end), awful.key({superkey, shiftkey}, "j", function(c)
        helpers.move_client_dwim(c, "down")
    end), awful.key({superkey, shiftkey}, "k", function(c)
        helpers.move_client_dwim(c, "up")
    end), awful.key({superkey, shiftkey}, "h", function(c)
        helpers.move_client_dwim(c, "left")
    end), awful.key({superkey, shiftkey}, "l", function(c)
        helpers.move_client_dwim(c, "right")
    end), awful.key({superkey}, "c", function(c)
        awful.placement.centered(c, {
            honor_workarea = true,
            honor_padding = true
        })
        helpers.single_double_tap(nil, function()
            helpers.float_and_resize(c, screen_width * 0.65, screen_height * 0.9)
        end)
    end), awful.key({superkey, shiftkey, ctrlkey}, "j", function(c)
        c:relative_move(0, dpi(20), 0, 0)
    end), awful.key({superkey, shiftkey, ctrlkey}, "k", function(c)
        c:relative_move(0, dpi(-20), 0, 0)
    end), awful.key({superkey, shiftkey, ctrlkey}, "h", function(c)
        c:relative_move(dpi(-20), 0, 0, 0)
    end), awful.key({superkey, shiftkey, ctrlkey}, "l", function(c)
        c:relative_move(dpi(20), 0, 0, 0)
    end), awful.key({superkey, shiftkey, ctrlkey}, "Down", function(c)
        c:relative_move(0, dpi(20), 0, 0)
    end), awful.key({superkey, shiftkey, ctrlkey}, "Up", function(c)
        c:relative_move(0, dpi(-20), 0, 0)
    end), awful.key({superkey, shiftkey, ctrlkey}, "Left", function(c)
        c:relative_move(dpi(-20), 0, 0, 0)
    end), awful.key({superkey, shiftkey, ctrlkey}, "Right", function(c)
        c:relative_move(dpi(20), 0, 0, 0)
    end), awful.key({superkey}, "t", function(c)
        decorations.cycle(c)
    end, {
        description = "toggle titlebar",
        group = "client"
    }), awful.key({superkey, shiftkey}, "t", function(c)
        local clients = awful.screen.focused().clients
        for _, c in pairs(clients) do
            decorations.cycle(c)
        end
    end, {
        description = "toggle titlebar",
        group = "client"
    }), awful.key({superkey, ctrlkey}, "f", function(c)
        helpers.float_and_resize(c, screen_width * 0.7, screen_height * 0.75)
    end, {
        description = "focus mode",
        group = "client"
    }), awful.key({superkey, ctrlkey}, "v", function(c)
        helpers.float_and_resize(c, screen_width * 0.45, screen_height * 0.90)
    end, {
        description = "focus mode",
        group = "client"
    }), awful.key({superkey, ctrlkey}, "t", function(c)
        helpers.float_and_resize(c, screen_width * 0.3, screen_height * 0.35)
    end, {
        description = "tiny mode",
        group = "client"
    }), awful.key({superkey, ctrlkey}, "n", function(c)
        helpers.float_and_resize(c, screen_width * 0.45, screen_height * 0.5)
    end, {
        description = "normal mode",
        group = "client"
    }), awful.key({altkey}, "F4", function(c)
        c:kill()
    end, {
        description = "close",
        group = "client"
    }), awful.key({superkey, ctrlkey}, "space", function(c)
        local layout_is_floating = (awful.layout.get(mouse.screen) == awful.layout.suit.floating)
        if not layout_is_floating then
            awful.client.floating.toggle()
        end
    end, {
        description = "toggle floating",
        group = "client"
    }), awful.key({ctrlkey, superkey}, "o", function(c)
        c.opacity = c.opacity - 0.1
    end, {
        description = "decrease client opacity",
        group = "client"
    }), awful.key({superkey, shiftkey}, "o", function(c)
        c.opacity = c.opacity + 0.1
    end, {
        description = "increase client opacity",
        group = "client"
    }), awful.key({superkey, shiftkey}, "p", function(c)
        c.ontop = not c.ontop
    end, {
        description = "toggle keep on top",
        group = "client"
    }), awful.key({superkey, ctrlkey}, "p", function(c)
        c.sticky = not c.sticky
    end, {
        description = "toggle sticky",
        group = "client"
    })})
end)

