require('keys.client_keys');
require('keys.client_keys')
require('keys.mouse_keys')
require('keys.tag_keys')
require('keys.global_keys')
local gears = require("gears")
local awful = require("awful")
local helpers = require("helpers")
local keys = {}

-- Mouse buttons on the primary titlebar of the window
keys.titlebar_buttons = gears.table.join(awful.button({}, 1, function()
    local c = mouse.object_under_pointer()
    client.focus = c
    awful.mouse.client.move(c)
end), awful.button({}, 2, function()
    local c = mouse.object_under_pointer()
    c:kill()
end), awful.button({}, 3, function()
    local c = mouse.object_under_pointer()
    client.focus = c
    awful.mouse.client.resize(c)
    -- awful.mouse.resize(c, nil, {jump_to_corner=true})
end), awful.button({}, 9, function()
    local c = mouse.object_under_pointer()
    client.focus = c
    -- awful.placement.centered(c,{honor_padding = true, honor_workarea=true})
    c.floating = not c.floating
end), awful.button({}, 8, function()
    local c = mouse.object_under_pointer()
    client.focus = c
    c.ontop = not c.ontop
end))

-- }}}

-- Mouse buttons on a tag of the taglist widget
keys.taglist_buttons = gears.table.join(awful.button({}, 1, function(t)
    helpers.tag_back_and_forth(t.index)
end), awful.button({
    superkey
}, 1, function(t)
    if client.focus then
        client.focus:move_to_tag(t)
    end
end), awful.button({}, 3, function(t)
    if client.focus then
        client.focus:move_to_tag(t)
    end
end), awful.button({
    superkey
}, 3, function(t)
    if client.focus then
        client.focus:toggle_tag(t)
    end
end), awful.button({}, 4, function(t)
    awful.tag.viewprev(t.screen)
end), awful.button({}, 5, function(t)
    awful.tag.viewnext(t.screen)
end))

-- }}}

return keys
