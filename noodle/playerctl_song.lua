local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")

-- Set colors
local title_color = beautiful.playerctl_song_title_color or beautiful.wibar_fg
local artist_color = beautiful.playerctl_song_artist_color or beautiful.wibar_fg
local paused_color = beautiful.playerctl_song_paused_color or beautiful.normal_fg

local playerctl_title = wibox.widget {
    text = "---------",
    align = "center",
    valign = "center",
    widget = wibox.widget.textbox
}

local playerctl_artist = wibox.widget {
    text = "---------",
    align = "center",
    valign = "center",
    widget = wibox.widget.textbox
}

-- Main widget
local playerctl_song = wibox.widget {
    playerctl_title,
    playerctl_artist,
    layout = wibox.layout.fixed.vertical
}

local artist_fg
local artist_bg

awesome.connect_signal("bling::playerctl::status", function(status)
    if status then
        artist_fg = artist_color
        title_fg = title_color
    else
        artist_fg = paused_color
        title_fg = paused_color
    end
end)

awesome.connect_signal("bling::playerctl::title_artist_album", function(title, artist)
    -- Escape &'s
    title = string.gsub(title, "&", "&amp;")
    artist = string.gsub(artist, "&", "&amp;")

    playerctl_title.markup = "<span foreground='" .. title_fg .. "'>" .. title .. "</span>"
    playerctl_artist.markup = "<span foreground='" .. artist_fg .. "'>" .. artist .. "</span>"
end)

return playerctl_song
